﻿using RabbitMQ.Client;
using System;
using System.Text;

namespace SendMessage
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory();
            factory.HostName = "103.40.20.155";
            factory.UserName = "mayt";
            factory.Password = "";//填写密码
            factory.Port = AmqpTcpEndpoint.UseDefaultPort;
            factory.VirtualHost = "/";  //默认为"/"
            factory.Protocol = Protocols.DefaultProtocol;
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare("hello", false, false, false, null);
                    string message = "Hello World2";
                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish("", "hello", null, body);
                    Console.WriteLine(" set {0}", message);
                }
            }
        }
    }
}
